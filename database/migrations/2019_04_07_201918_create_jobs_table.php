<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('category_id');
            $table->text('company');
            $table->string('title');
            $table->text('description');
            $table->string('image');
            $table->integer('opening_job')->nullable();
            $table->string('location');
            // $table->boolean('hot_job');
            // $table->boolean('latest_job');
            // $table->boolean('available_job');




            // $table->string('available_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
