<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/', 'IndexController@index');
Route::match(['get', 'post'], '/adminLogin', 'AdminController@login')->name('admin.login');

//Password reset routes
Route::post('admin/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
Route::get('admin/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
Route::post('admin/password/reset', 'Auth\ResetPasswordController@reset')->name('admin.password.update');
Route::get('admin/password/reset/{token}','Auth\ResetPasswordController@showResetForm')->name('admin.password.reset');

Route::group(['as'=>'admin.' ,'middleware' => 'auth', 'prefix'=>'admin'], function(){
    // Admin Settings and Login
    Route::get('/dashboard', 'AdminController@dashboard')->name('dashboard');
    Route::get('/admin/profile/{id}', 'AdminController@profile')->name('profile');
    Route::post('/admin/update/profile/{id}', 'AdminController@updateProfile')->name('update.profile');
    Route::match(['get', 'post'], '/admin/edit/password', 'AdminController@editPassword')->name('edit.password');
    Route::post('/admin/edit/check-password', 'AdminController@chkUserPassword');

    //Categories
    Route::get('category/create', 'CategoryController@create')->name('category.create');
    Route::get('category/view', 'CategoryController@view')->name('category.view');
    Route::post('category/store', 'CategoryController@store')->name('category.store');
    Route::match(['get','post'],'edit-category/{id}','CategoryController@edit')->name('category.edit');
    Route::match(['get','post'],'delete-category/{id}','CategoryController@delete')->name('category.delete');
    Route::match(['get','post'],'trash-category/{id}','CategoryController@trash')->name('category.trash');

    //Jobs
    Route::match(['get','post'],'job/create','JobController@addJob')->name('job.create');
    Route::match(['get','post'],'job/view-jobs','JobController@viewJobs')->name('job.view');
    Route::match(['get','post'],'edit-job/{id}','JobController@edit')->name('job.edit');
    Route::match(['get','post'],'/delete-job/{id}','JobController@delete')->name('job.delete');
    Route::match(['get','post'],'delete-job-image/{id}','JobController@deleteJobImage')->name('job.image.delete');


    // Route::match(['get','post'],'trash-job/{id}','JobController@trash')->name('job.trash');











});



Route::get('/logout', 'AdminController@logout')->name('admin.logout');


Route::get('/home', 'HomeController@index')->name('home');

// Categories
// Route::group(['as'=>'admin.', 'middleware'=>['auth', 'admin'] , 'prefix'=>'admin'],function(){



// });